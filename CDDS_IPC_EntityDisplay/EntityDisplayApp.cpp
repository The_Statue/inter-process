#include "EntityDisplayApp.h"
#include <windows.h>

EntityDisplayApp::EntityDisplayApp(int screenWidth, int screenHeight) : m_screenWidth(screenWidth), m_screenHeight(screenHeight) {

}

EntityDisplayApp::~EntityDisplayApp() {

}

bool EntityDisplayApp::Startup() {

	InitWindow(m_screenWidth, m_screenHeight, "EntityDisplayApp");
	SetTargetFPS(60);

	h = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, L"EntityMemory");

	return true;
}

void EntityDisplayApp::Shutdown() {

	CloseHandle(h);
	CloseWindow();        // Close window and OpenGL context
}

void EntityDisplayApp::Update(float deltaTime) {

}

void EntityDisplayApp::Draw() {
	BeginDrawing();

	ClearBackground(RAYWHITE);

	char* data = (char*)MapViewOfFile(h, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(size_t));
	if (data)
	{
		size_t entityCount;
		memcpy(&entityCount, data, sizeof(size_t));
		UnmapViewOfFile(data);
		data = (char*)MapViewOfFile(h, FILE_MAP_ALL_ACCESS, 0, 0, sizeof(Entity) * entityCount + sizeof(size_t));
		if (data)
		{
			m_entities.resize(entityCount);
			memcpy(m_entities.data(), data + sizeof(size_t), sizeof(Entity) * entityCount);
			UnmapViewOfFile(data);
		}
	}

	// draw entities
	for (auto& entity : m_entities) {
		DrawRectanglePro(
			Rectangle{ entity.x, entity.y, entity.size, entity.size }, // rectangle
			Vector2{ entity.size / 2, entity.size / 2 }, // origin
			entity.rotation,
			Color{ entity.r, entity.g, entity.b, 255 });
	}

	// output some text, uses the last used colour
	DrawText("Press ESC to quit", 630, 15, 12, LIGHTGRAY);

	EndDrawing();
}